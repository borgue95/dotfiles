# Dotfiles

To install them on a fresh arch installation:
1. Clone the repo on your home directory, whatever you want.
2. Go inside the folder and execute the `install.sh` script. It must be
   executed from the repository root folder. 
3. Then, if you want to change the locale to a custom one, execute the same
   script as root using `sudo`.
4. Then, execute `install.sh fix-monitors` to try to fix the monitor layout in
   all the config files that require them.

To install them on a non fresh arch installation:
1. Make sure that all the files/folders that the `install.sh` script links are
   not present (move them, one by one, or delete them)
2. Then, follow the above steps

## To make a fresh install of arch

Follow my simplified instructions on this [snipped](https://gitlab.com/-/snippets/1942714)
