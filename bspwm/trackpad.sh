trackpadid=$(xinput list | grep bcm5974 | awk '{print $4}' | cut -d= -f2)

tap_enable=$(xinput list-props $trackpadid | grep "Tapping Enabled (" | grep -o -P --colour '\(\K\d*')
xinput set-prop $trackpadid $tap_enable 1
scroll_natural=$(xinput list-props $trackpadid | grep "Natural Scrolling Enabled (" | grep -o -P --colour '\(\K\d*')
xinput set-prop $trackpadid $scroll_natural 0
