#!/bin/sh
xrandr \
  --output DP-1 --off \
  --output HDMI-1 --off \
  --output DP-2 --off \
  --output HDMI-2 --off \
  --output eDP-1 --mode 1440x900 --pos 0x0 --rotate normal --primary
