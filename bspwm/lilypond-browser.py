import sys
import os
def main():
    args = sys.argv
    uri = args[1]
    file = uri.split(':')[1][2:]
    line = uri.split(':')[2]
    column = int(uri.split(':')[3])+1
    cmd="vim --servername SERVER --remote \"+call cursor("+str(line)+", "+str(column)+")\" "+str(file)
    os.system(cmd)
    if len(args) == 3:
        cmd="bspc node "+str(args[2])+" --focus"
        os.system(cmd)


if __name__ == "__main__":
    main()
