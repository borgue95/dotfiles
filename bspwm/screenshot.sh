#!/bin/bash

data=$(date +%Y%m%d_%H%M%S)
filename=$data.png
mkdir -p $(xdg-user-dir PICTURES)/screenshots
path=$(xdg-user-dir PICTURES)/screenshots
route=$path/$filename

# route inside " " for supporting path with spaces
import "$route"
