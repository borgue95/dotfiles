#bspc rule -a URxvt state=floating -o rectangle=300x200 sticky=off
myid=$(bspc query -N -n)

urxvt -name lilycompile -e sh -c "ls *.ly | entr -c lilypond /_" &
#sleep 2
lilyID=$(xdotool search --sync --classname "lilycompile")
bspc node $lilyID -d 9
BROWSER="python $HOME/.config/bspwm/lilypond-browser.py %s $myid" zathura &
vim --servername SERVER
