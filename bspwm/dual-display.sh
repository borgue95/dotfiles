dual=$1

if [[ $HOSTNAME != "march" ]]; then
    echo "Only laptop"
    exit 1
fi

if [[ "$dual" == "true" ]]
then
    # change screenlayout.sh
    ~/.config/bspwm/screenlayout-march-dual.sh
    bspc monitor HDMI-1 -d 10 11 12 13 14 15 16

    # enable polybar
    killall -q polybar
    while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
    polybar masterlaptop &
    polybar slavelaptop &

    # enable bspwm desktops

    # rerun xwallpaper
    xwallpaper --zoom $HOME/.config/bg

else
    # change screenlayout.sh
    ~/.config/bspwm/screenlayout-march-single.sh
    bspc monitor HDMI-1 -r

    # enable polybar
    killall -q polybar
    while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
    polybar masterlaptop &

    # rerun xwallpaper
    xwallpaper --zoom $HOME/.config/bg
fi


