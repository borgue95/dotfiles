if [[ $HOSTNAME == "march" ]]; then
    $HOME/.config/bspwm/trackpad.sh
    $HOME/apps/src/iris-floss/iris-floss 4200 100
    $HOME/.config/bspwm/screenlayout-mac-single.sh
else
    # they say reduce screen tearing in Xorg...
    nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }"
    $HOME/apps/src/iris-floss/iris-floss 4200 75
    $HOME/.config/bspwm/screenlayout.sh
fi

$HOME/.config/polybar/launch.sh

xwallpaper --zoom $HOME/.config/bg

wmname LG3D
setxkbmap es
xset b off
xset r rate 250 50
xdotool mousemove --screen 0 960 540
[[ "$(xset q | grep Num | awk '{print $8}')" == "off" ]] && xdotool key Num_Lock

picom &
nextcloud &
nm-applet &
dunst -config $HOME/.config/dunst/dunstrc &
[[ -z $(ps aux | grep sxhkd | grep -v grep) ]] && sxhkd &
[[ -z $(ps aux | grep pasystray | grep -v grep) ]] && pasystray &

