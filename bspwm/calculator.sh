#!/bin/bash

calculatorid=$(xdotool search --name calculadora)
if [ -n "$calculatorid" ]
then
    calculatorid=$(bspc query -N -n $calculatorid)
    bspc node $calculatorid --flag hidden --focus
else
    bspc rule -a URxvt state=floating -o rectangle=300x200 center=on sticky=on
    urxvt -title calculadora -e python &
    calculatorid=$(xdotool search --name calculadora)
    bspc node $calculatorid --focus
fi


