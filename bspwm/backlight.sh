#!/bin/bash

current=$(cat /sys/class/backlight/acpi_video0/brightness)

if [[ -n "$1" && "$1" == "inc" ]]
then
    if [ "$current" -le "95" ]
    then
        new=$(echo $current + 5 | bc)
        echo $new > /sys/class/backlight/acpi_video0/brightness
    fi
elif [[ -n "$1" && "$1" == "dec" ]]
then
    if [ "$current" -ge "5" ]
    then
        new=$(echo $current - 5 | bc)
        echo $new > /sys/class/backlight/acpi_video0/brightness
    fi
fi


