[colors]
# fons
background = #88222222
# fons seleccionat
background-alt = #B0222222
# text
foreground = #fefefe
# text deseleccionat
foreground-alt = #afafaf
# underline 1
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/example]
width = 100%
height = 23
radius = 0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = fixed:pixelsize=11;1
font-1 = "Noto Sans"
font-2 = FontAwesome5Free:style=Solid:size=10;1

modules-left = bspwm
modules-center = date

tray-padding = 2

wm-restack = bspwm
override-redirect = true

cursor-click = pointer

[bar/master]
tray-position = right
inherit = bar/example

[bar/slave]
inherit = bar/example

[bar/masterlaptop]
inherit = bar/master
modules-right = battery filesystem alsa ethlaptop cpu memory temperature powermenu
monitor = 

[bar/masterdesktop]
inherit = bar/master
modules-right = filesystem alsa ethdesktop cpu memory temperature powermenu
monitor = HDMI-0


[bar/slavelaptop]
inherit = bar/slave
modules-right = battery filesystem alsa ethlaptop cpu memory temperature powermenu
monitor = 

[bar/slavedesktop]
inherit = bar/slave
modules-right = filesystem alsa ethdesktop cpu memory temperature powermenu
monitor = DVI-I-1

;==============================================================================

[module/battery]
type = internal/battery
full-at = 99
battery = BAT0
adapter = ADP1
poll-interval = 5

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %percentage_used%%
format-mounted = <label-mounted>
format-mounted-prefix = " "
format-mounted-prefix-foreground = ${colors.foreground-alt}

label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %name%
label-occupied-padding = 2

label-urgent = %name%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %name%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

reverse-scroll = true

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

label-song-maxlen = 25
label-song-ellipsis = true

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.primary}
label = %percentage%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.primary}
label = %percentage_used%%

[module/ethlaptop]
type = internal/network
interface = wlp3s0
interval = 3.0

format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %upspeed:9%  %downspeed:9% 

format-disconnected =

[module/ethdesktop]
type = internal/network
interface = eno1
interval = 3.0

format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %upspeed:9%  %downspeed:9% 

format-disconnected =

[module/date]
type = internal/date
interval = 1

date = " %d/%m/%y"
date-alt = " %d/%m/%y"

time = %H:%M:%S
time-alt = %H:%M:%S

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}

label = %time% %date% 

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = -
bar-volume-fill-font = 2
bar-volume-empty = -
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <ramp-volume> <bar-volume>
label-volume = 
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = muted

bar-volume-width = 10
bar-volume-foreground-0 = #70ba26
bar-volume-foreground-1 = #70ba26
bar-volume-foreground-2 = #70ba26
bar-volume-foreground-3 = #95ba26
bar-volume-foreground-4 = #baba26
bar-volume-foreground-5 = #ba9526
bar-volume-foreground-6 = #ba4b26
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = -
bar-volume-fill-font = 1
bar-volume-empty = -
bar-volume-empty-font = 1
bar-volume-empty-foreground = ${colors.foreground-alt}

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

[module/temperature]
type = internal/temperature
thermal-zone = 1
hwmon-pah = /sys/devices/platform/coretemp.0/hwmon/hwmon2/temp1_input
base-temperature = 38
warn-temperature = 65
interval = 2

format = <ramp> <label>
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}
format-underline = ${colors.primary}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-0-foreground = #95ba26
ramp-1-foreground = #ba9526
ramp-2-foreground = #ba2626

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo shutdown -h now
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

