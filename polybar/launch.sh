#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if [[ $HOSTNAME == "march" ]]; then
    polybar masterlaptop &
else
    polybar masterdesktop &
fi

num_displays=$(xrandr | grep -e " connected " | wc -l)
if [[ $num_displays -gt 1 ]]; then
    if [[ $HOSTNAME == "march" ]]; then
        polybar slavelaptop &
    else
        polybar slavedesktop &
    fi
fi

