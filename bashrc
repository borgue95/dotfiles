#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

reset="\e[39m"
lgray="\e[37m"
lred="\e[91m"
lgreen="\e[92m"
lblue="\e[94m"

PS1="\[$lgray\][\[$lgreen\]\u\[$lgray\]@\[$lred\]\h \[$lblue\]\W\[$lgray\]]\$\[$reset\] "

# enable bash completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

alias ls='ls --color=auto'
alias l='ls -l'
alias la='ls -la'

# XDG alias
alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
alias dunst='dunst -config $XDG_CONFIG_HOME/dunst/dunstrc'

# customizing bash history
export HISTTIMEFORMAT="%h %d %H:%M:%S "
export HISTSIZE=10000000
export HISTFILESIZE=50000000
export HISTCONTROL=ignoredups

tput smkx

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH
