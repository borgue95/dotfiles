#!/bin/bash

if [[ -z $2 ]]; then
    echo "usage: $0 path input-format quality"
    echo "input-format. three letter extension, like \"wav\""
    echo "quality (same as libmp3lame in FFmpeg): \"-b:a 320k\" insane, \"-q:a 0\" great, \"-q:a 9\" worse"
    echo
    echo "This script will search for *.\$input-format files inside the \$path provided and "
    echo "convert them into mp3 at given \$quality."
    echo "The utility will exclude the files starting with \"STE\"."
    exit 1
fi

carpeta=$1
inputFormat=$2
qualitat=$3

for f in "$carpeta"/*.$inputFormat
do
    tmp=$(basename "$f")
    if [[ ${tmp:0:3} != "STE" ]]
    then
        if [ ! -f ${f:0:-4}.mp3 ]
        then
            ffmpeg -i "$f" -codec:a libmp3lame $qualitat "${f:0:-4}.mp3"
        fi
    fi
done


