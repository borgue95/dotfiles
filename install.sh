#!/bin/bash

if [[ -n "$1" && "$1" == "fix-monitors" ]]
then
    # bspwm
    #mv bspwm/bspwmrc bspwm/bspwmrc.bk
    # polybar
    #mv polybar/config polybar/config.bk

    # screenlayout
    printf '%s\n' "#!/bin/sh" > bspwm/screenlayout.sh
    printf '%s\\\n' "xrandr " >> bspwm/screenlayout.sh
    displays=$(xrandr --current | tail -n +2 | grep disconnected | awk '{print $1}')
    for d in ${displays}; do
        printf '  %s \\\n' "--output ${d} --off" >> bspwm/screenlayout.sh
    done
    displays=$(xrandr --current | tail -n +2 | grep " connected")
    echo "$displays"
    #for d in "${displays}"; do
    while read -r d; do
        name=$(echo $d | awk '{print $1}')
        primary=$(echo $d | cut -d\  -f 3)
        if [[ $primary == "primary" ]]
        then
            resolution=$(echo $d | cut -d\  -f 4 | cut -d+ -f 1)
            printf '  %s\\\n' "--output ${name} --mode ${resolution} --pos 0x0 --rotate normal --dpi 96 --primary" >> bspwm/screenlayout.sh
            # s'ha de ser més smart perquè s'ha de diferenciar entre laptop i desktop
            #sed "s/bspc monitor \b[[:alnum:]\-]*\b/bspc monitor ${name}/g" bspwm/bspwmrc.bk > bspwm/bspwmrc
            #sed "s/^monitor = .*/monitor = ${name}/g" polybar/config.bk > polybar/config
        else
            resolution=$(echo $d | cut -d\  -f 3 | cut -d+ -f 1)
            printf '  %s\\\n' "--output ${name} --mode ${resolution} --pos 1920x0 --rotate normal" >> bspwm/screenlayout.sh
            # s'ha de ser més smart perquè s'ha de diferenciar entre laptop i desktop
            #sed "s/bspc monitor \b[[:alnum:]\-]*\b/bspc monitor ${name}/g" bspwm/bspwmrc.bk > bspwm/bspwmrc
            #sed "s/^monitor = .*/monitor = ${name}/g" polybar/config.bk > polybar/config
        fi
    done < <(echo "$displays")
    printf '\n' >> bspwm/screenlayout.sh

    chmod u+x bspwm/screenlayout.sh
    chmod u+x bspwm/bspwmrc

    bspc wm -r

    exit 0
fi

if [ "$EUID" -eq 0 ]
then 
    echo "Running as root. Performing only the operations that need root access"
    cp ca_ES /usr/share/i18n/locales/
    locale-gen
    exit 0
fi

CONFIG="${HOME}/.config"
mkdir -p ${CONFIG}

mkdir ~/docs
mkdir ~/down
mkdir ~/music
mkdir ~/pics
mkdir ~/tmp
mkdir ~/videos
mkdir ~/apps/bin

#ln -sf $(pwd)/bspwm ${CONFIG}
ln -sf $(pwd)/dunst ${CONFIG}
ln -sf $(pwd)/dwm ${CONFIG}
ln -sf $(pwd)/fontconfig ${CONFIG}
ln -sf $(pwd)/picom ${CONFIG}
#ln -sf $(pwd)/polybar ${CONFIG}
#ln -sf $(pwd)/sxhkd ${CONFIG}
ln -sf $(pwd)/vifm ${CONFIG}
ln -sf $(pwd)/sxiv ${CONFIG}
ln -sf $(pwd)/urxvt ${HOME}/.urxvt
ln -sf $(pwd)/user-dirs.dirs ${CONFIG}

ln -sf $(pwd)/Xdefaults ~/.Xdefaults
ln -sf $(pwd)/bash_profile ~/.bash_profile
ln -sf $(pwd)/bashrc ~/.bashrc
ln -sf $(pwd)/ideavimrc ~/.ideavimrc
ln -sf $(pwd)/vimrc ~/.vimrc
ln -sf $(pwd)/xinitrc ~/.xinitrc

ln -sf $(pwd)/scripts/convert-to-mp3.sh ~/apps/bin

# pull st, dwm and dwmblocks
mkdir -p ~/repos
cd ~/repos
git clone https://gitlab.com/borgue95/dwm.git
git clone https://gitlab.com/borgue95/st.git
git clone https://gitlab.com/borgue95/dwmblocks.git

cd dwm
make PREFIX=~/apps install
cd ../st
make PREFIX=~/apps install
cd ../dwmblocks
make PREFIX=~/apps install

