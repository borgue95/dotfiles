#
# ~/.bash_profile
#

export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_QPA_PLATFORM_PLUGIN_PATH=/usr/lib/qt/plugins/platforms
export _JAVA_AWT_WM_NONREPARENTING=1
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export HOME_APPS="$HOME/apps"
export HOME_REPOS="$HOME/repos"

# XDG apps
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export DIALOGRC="$XDG_CONFIG_HOME/dialog/dialogrc"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export DVDCSS_CACHE="$XDG_DATA_HOME"/dvdcss
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority

 #texlive
if [[ $HOSTNAME == "march" ]]; then
    export MANPATH="$HOME_APPS/texlive/2021/texmf-dist/doc/man:$MANPATH"
    export INFOPATH="$HOME_APPS/texlive/2021/texmf-dist/doc/info:$INFOPATH"
    export PATH="$HOME_APPS/texlive/2021/bin/x86_64-linux:$PATH"
else
    export MANPATH="$HOME_APPS/texlive/2024/texmf-dist/doc/man:$MANPATH"
    export INFOPATH="$HOME_APPS/texlive/2024/texmf-dist/doc/info:$INFOPATH"
    export PATH="$HOME_APPS/texlive/2024/bin/x86_64-linux:$PATH"
fi

export PATH="$HOME_APPS/bin:$PATH"
export PATH="$HOME_REPOS/make-backups:$PATH"
export PATH="$HOME_REPOS/little_random_programs/photos:$PATH"
export PATH="$HOME_REPOS/little_random_programs/recording:$PATH"
export PATH="$HOME_REPOS/little_random_programs/system:$PATH"
export PATH="$XDG_CONFIG_HOME/dwm:$PATH"
export PATH="$XDG_CONFIG_HOME/dwm/scripts:$PATH"
export PATH="$XDG_CONFIG_HOME/dwm/statusbar:$PATH"
export LD_LIBRARY_PATH="$HOME/apps/lib:$LD_LIBRARY_PATH"

# Install Ruby Gems to ~/gems
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$GEM_HOME/bin:$PATH"

export MOZ_X11_EGL=1
export TESSDATA_PREFIX="/usr/share/tessdata"
#source /usr/share/nvm/init-nvm.sh

[[ -f ~/.bashrc ]] && . ~/.bashrc

