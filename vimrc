set backspace=indent,eol,start
set nocompatible              " be iMproved, required
filetype off                  " required
set runtimepath+=/usr/share/vim/vimfiles

" VUNDLE CONFIG
" -----------------------------------------------------------------------------

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

" let Vundle manage Vundle, required
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'morhetz/gruvbox'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'junegunn/goyo.vim'
Plugin 'scrooloose/nerdtree'
call vundle#end()
" -----------------------------------------------------------------------------

filetype on
filetype plugin on
filetype plugin indent on

" Disable visual bell completelly
set t_vb= 

" enable syntax highlighting
syntax on
syntax enable
 
" PLUGIN CONFIGS
" -----------------------------------------------------------------------------

" GRUVBOX config
let g:gruvbox_contrast_dark="soft"
set background=dark
colorscheme gruvbox

" Goyo config
let g:goyo_width=120

" F-MAPPINGS
" -----------------------------------------------------------------------------
map <F1> :LLPStartPreview<CR>
map <F2> :NERDTreeToggle<CR>
nnoremap <F3> :set hlsearch!<CR>       " Toggle visibility of search results

"au BufNewFile,BufRead *.ly set filetype=lilypond
" LaTeX basic compilation
autocmd FileType tex,markdown map <F5> :w<CR>:!make<CR><CR>
"autocmd FileType lilypond map <F5> :!lilypond % <CR><CR>
map <F6> :setlocal spell! spelllang=ca<CR>
map <F7> gwip                          " wrap text inside the paragraph
map <F8> vipJ                          " unwrap text inside the paragraph
map <F9> :Goyo<CR>                     " Toggle Goyo

" VIM CONFIG
" -----------------------------------------------------------------------------
vnoremap <C-c> "+y
map <C-v> "+p

au BufNewFile,BufRead *.cl set filetype=c
au BufNewFile,BufRead *.lytex set filetype=tex

set number relativenumber
set t_Co=256

let g:spellfile_URL = 'http://ftp.vim.org/vim/runtime/spell'
autocmd FileType tex,markdown,text set textwidth=100  " autowrap at 100 characters
autocmd FileType tex,markdown,text set colorcolumn=100

" Split navigation config
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-h> <C-w>h
map <C-l> <C-w>l

" Set line breaker indicator
set colorcolumn=80,120

set tabstop=4     " tab character width
set shiftwidth=4  " what happens when ==, << or >>
set softtabstop=4 " how many spaces when tabbing
set smarttab      " begin counting from the begining of the line
set expandtab     " Use spaces instead of tab character

set autoindent    " copy last line indentation
set smartindent   " indentation based on syntax
set wrap          " wrap lines
set linebreak     " wrap lines without breaking words

" Ignore case when searching
set ignorecase
" Highlight all search pattern matches
set hlsearch

" Use mouse to move inside vim
"set mouse=a

set scrolloff=5

" FINDING FILES
" Search down into subfolders
set path+=**

" Display all matching files when I tab complete
set wildmenu

" TEX MACROS
" -----------------------------------------------------------------------------
" Navigation
" yanks the <++> pattern to the _ register
autocmd FileType tex inoremap <Tab><Tab> <Esc>/<++><CR>"_ca<
autocmd FileType tex vnoremap <Tab><Tab> <Esc>/<++><CR>
autocmd FileType tex nnoremap <Tab><Tab> <Esc>/<++><CR>"_ca<
autocmd FileType tex inoremap ;gui <++>

" Shortcuts
autocmd FileType tex inoremap ;bf \textbf{}<++><Esc>T{i
autocmd FileType tex inoremap ;tt \texttt{}<++><Esc>T{i
autocmd FileType tex inoremap ;it \textit{}<++><Esc>T{i
autocmd FileType tex inoremap ;sc \textsc{}<++><Esc>T{i
autocmd FileType tex inoremap ;qu \enquote{}<++><Esc>T{i
autocmd FileType tex inoremap ;sec \section{}<CR><CR><++><Esc>2k0f{a
autocmd FileType tex inoremap ;ssec \subsection{}<CR><CR><++><Esc>2k0f{a
autocmd FileType tex inoremap ;sssec \subsubsection{}<CR><CR><++><Esc>2k0f{a

autocmd FileType tex inoremap ;en \begin{enumerate}<CR><CR>\item<Space><CR><CR>\end{enumerate}<CR><CR><++><Esc>4kA
autocmd FileType tex inoremap ;po \begin{itemize}<CR><CR>\item<Space><CR><CR>\end{itemize}<CR><CR><++><Esc>4kA
autocmd FileType tex inoremap ;im \begin{figure}<CR>\cCRing<CR>\includegraphics[]{<++>}<CR>\caption{<++>\label{<++>}}<CR>\end{figure}<CR><CR><++><Esc>4k0<Esc>f[a
autocmd FileType tex inoremap ;beg \begin{DELRN}<CR><++><CR>\end{DELRN}<CR><CR><++><Esc>4k0fR:MultipleCursorsFind<Space>DELRN<CR>c

" Search in multiple lines
" Search for the ... arguments separated with whitespace (if no '!'),
" or with non-word characters (if '!' added to command).
function! SearchMultiLine(bang, ...)
  if a:0 > 0
    let sep = (a:bang) ? '\_W\+' : '\_s\+'
    let @/ = join(a:000, sep)
  endif
endfunction
command! -bang -nargs=* -complete=tag S call SearchMultiLine(<bang>0, <f-args>)|normal! /<C-R>/<CR>

" C/C++ MACROS

