#!/bin/bash

firstLevel="Do nothing\nExit\nShut down\nReboot"
selected=$(printf "$firstLevel" | dmenu -i -p "Actions:")

if [[ "$selected" == "Do nothing" ]]; then 
    echo "Do nothing"

elif [[ "$selected" == "Exit" ]]; then 
    selected=$(printf "No\\nYes" | dmenu -i -p "Do you want to exit?")
    [[ "$selected" == "Yes" ]] && pkill dwm || echo "No exit"

elif [[ "$selected" == "Shut down" ]]; then 
    selected=$(printf "No\\nYes" | dmenu -i -p "Do you want to shut down?")
    [[ "$selected" == "Yes" ]] && shutdown now || echo "No shut down"

elif [[ "$selected" == "Reboot" ]]; then
    selected=$(printf "No\\nYes" | dmenu -i -p "Do you want to reboot?")
    [[ "$selected" == "Yes" ]] && reboot || echo "No reboot"
fi

