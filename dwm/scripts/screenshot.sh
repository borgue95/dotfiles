#!/bin/bash

data=$(date +%Y%m%d_%H%M%S)
filename=$data.png
path=$(xdg-user-dir PICTURES)/screenshots
mkdir -p "$path"
route="$path/$filename"

if [[ "$1" == "crop" ]]; then
    import "$route"
elif [[ "$1" == "window" ]]; then
    import -window "$(xdotool getwindowfocus -f)" "$route"
else 
    import -window root "$route"
fi
