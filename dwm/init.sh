if [[ $HOSTNAME == "march" ]]; then
    $HOME/apps/src/iris-floss/iris-floss 4900 100
    $XDG_CONFIG_HOME/bspwm/trackpad.sh
    $XDG_CONFIG_HOME/bspwm/screenlayout-mac-single.sh
    [[ -z $(ps aux | grep sxhkd | grep -v grep) ]] && sxhkd &
else
    # they say reduce screen tearing in Xorg...
    #nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }"
    $HOME/apps/src/iris-floss/iris-floss 5500 60
    $XDG_CONFIG_HOME/dwm/scripts/screenlayout.sh
fi

# $HOME/.config/polybar/launch.sh
dwmblocks &
xwallpaper --zoom $XDG_CONFIG_HOME/bg

# wmname LG3D
setxkbmap -option compose:menu-altgr es
xset b off
xset r rate 250 50
xdotool mousemove --screen 0 960 540
[[ "$(xset q | grep Num | awk '{print $8}')" == "off" ]] && xdotool key Num_Lock

picom &
nextcloud --background &
nm-applet &
dunst &
[[ -z $(ps aux | grep pasystray | grep -v grep) ]] && pasystray &

