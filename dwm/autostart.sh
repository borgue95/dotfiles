#!/bin/sh
$HOME/.config/dwm/init.sh

# to auto reload dwm
while true; do
    $HOME/apps/bin/dwm > /dev/null 2> /tmp/dwm.log && continue || break 
done
