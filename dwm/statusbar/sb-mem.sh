#!/bin/sh

mem=$(free -h | tail -n 2 | head -n 1)
used=$(echo $mem | awk '{print $3}')
free=$(echo $mem | awk '{print $4}')
a="      Utilitzat    Lliure
"
a=$a"RAM: $(printf '%10s' $used $free)
"
mem=$(free -h | tail -n 1 | head -n 1)
used=$(echo $mem | awk '{print $3}')
free=$(echo $mem | awk '{print $4}')
a=$a"SWAP:$(printf '%10s' $used $free)"

case $BLOCK_BUTTON in
    [1-6]) notify-send --app-name "Memòria RAM" --urgency="low" "$a" ;;
esac

echo "" $(free -h | tail -n 2 | head -n 1 | awk '{print $3}')

