#!/bin/sh

case $BLOCK_BUTTON in
    1) notify-send --app-name="Calendari" --urgency=low "$(cal -3 | head -n -1)" ;;
esac

echo  $(date +"%d/%m/%y %H:%M:%S")
