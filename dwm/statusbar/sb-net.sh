#!/bin/bash

path="/sys/class/net"

# exclude loopback and pick the first one
interface=$(ls $path | grep -v lo | head -n 1)  
[ -z $interface ] && { printf " \n" ; exit 0; }

status=$(cat "$path/$interface"/operstate)
ip=$(ip -o -4 a | grep -v ": lo" | head -n 1 | cut -d\  -f 7 | cut -d/ -f 1)
[ -z $ip ] && { printf " %s" $status ; exit 0; }

# LOAD
before=$(cat $XDG_CACHE_HOME/statusbar-net-previous)
rx_bytes_before=${before%% *}
tx_bytes_before=${before##* }
rx_bytes_after=$(($(cat $path/*/statistics/rx_bytes | paste -sd '+')))
tx_bytes_after=$(($(cat $path/*/statistics/tx_bytes | paste -sd '+')))
echo "$rx_bytes_after" "$tx_bytes_after" > $XDG_CACHE_HOME/statusbar-net-previous
if [ -z "$rx_bytes_before" ]; then
    exit
fi

rx_bytes_diff=$((($rx_bytes_after - $rx_bytes_before) / 1024 / 2))
tx_bytes_diff=$((($tx_bytes_after - $tx_bytes_before) / 1024 / 2))

[ "$rx_bytes_diff" -ge "999" ] && rx_bytes_diff=$(($rx_bytes_diff / 1024)) ru="M" || ru="K"
[ "$tx_bytes_diff" -ge "999" ] && tx_bytes_diff=$(($tx_bytes_diff / 1024)) tu="M" || tu="K"

printf " %s %3s %sB/s  %3s %sB/s \n" $ip $tx_bytes_diff $tu $rx_bytes_diff $ru
