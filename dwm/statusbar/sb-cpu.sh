#!/bin/bash

case $BLOCK_BUTTON in
    1)
        notify-send "butó 1"
        ;;
    2) notify-send "butó 2" ;;
    3) notify-send "butó 3" ;;
    4)
        notify-send "butó 4"
        ;;
    5)
        notify-send "butó 5"
        ;;
    6) notify-send "butó 6" ;;
esac

# TEMP
temp1=$(sensors | grep "Package id 0:" | awk '{print $4}')
temp1=${temp1:1}
temp1=${temp1:0:2}${temp1:4:2}

# FREQUENCY
#count=$(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq | wc -l)
#freq=$(cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq | paste -sd+ | bc)
#freq=$(echo "scale=9; $freq / $count / 1000000" | bc | awk '{printf "%.1f", $0}')
#freq=$freq"GHz"

# LOAD
prev=$(cat $XDG_CACHE_HOME/statusbar-cpu-previous)
curr=$(cat /proc/stat | grep "cpu ")
echo "$curr" > $XDG_CACHE_HOME/statusbar-cpu-previous
if [ -z "$prev" ]; then
    exit
fi

PrevIdle=$(echo $prev  | awk '{print $5+$6}')
CurrIdle=$(echo $curr  | awk '{print $5+$6}')
PrevTotal=$(echo $prev | awk '{print $2+$3+$4+$5+$6+$7+$8+$9}')
CurrTotal=$(echo $curr | awk '{print $2+$3+$4+$5+$6+$7+$8+$9}')

cpu_usage=$(echo "scale=6; 100.0 * (1.0 - ($CurrIdle - $PrevIdle) / ($CurrTotal - $PrevTotal))" | bc | awk '{printf "%.0f", $0}')

printf " %3s%% %s\n" $cpu_usage $temp1
