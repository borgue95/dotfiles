#!/bin/bash

case $BLOCK_BUTTON in
    1) amixer -q -D pulse sset Master toggle ;;
    4) amixer -q -D pulse sset Master 5%+ ;;
    5) amixer -q -D pulse sset Master 5%- ;;
esac

muted=$(amixer -D pulse | grep "Front Left:" | head -n 1 | cut -d\  -f 8)
[[ "$muted" == "[off]" ]] && { echo " Muted"; exit 0; }

current=$(amixer -D pulse | grep "Front Left:" | head -n 1 | cut -d\  -f 7)
current=${current:1:-1}
echo " $current"


